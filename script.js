const seguros = [
  { id: 1, nombre: 'Básico', valor: 500 },
  { id: 2, nombre: 'Intermedio', valor: 1000 },
  { id: 3, nombre: 'Premium', valor: 1500 },
];

function getTipoSeguro() {
  var tipo_seguro_id = document.getElementById('tipo_seguro_id').value;
  //busco el tipo de seguro correspondiente
  const seguro_seleccionado = findTipoSeguro(tipo_seguro_id);
  if (!seguro_seleccionado) {
    return;
  }
  //imprimo en pantalla el valor
  document.getElementById(
    'mensaje_costo_seguro'
  ).innerHTML = `El costo del seguro ${seguro_seleccionado.nombre} es de $ ${seguro_seleccionado.valor}`;
}

function findTipoSeguro(id) {
  return seguros.filter((seguro) => seguro.id == id)[0];
}

function submitForm(event) {
  event.preventDefault();

  const nombre = document.form_contacto.nombre.value;
  const apellido = document.form_contacto.apellido.value;
  const dni = document.form_contacto.dni.value;
  const email = document.form_contacto.email.value;
  const telefono = document.form_contacto.telefono.value;
  const tipo_seguro_id = document.form_contacto.tipo_seguro.value;
  const seguro = findTipoSeguro(tipo_seguro_id);

  const msgData = `
    -- Datos del formulario -- 
    Nombre: ${nombre}
    Apellido: ${apellido}
    DNI: ${dni}
    Email: ${email}
    Teléfono: ${telefono}
    Tipo seguro: ${seguro && seguro.nombre}
    Valor del seguro: $ ${seguro && seguro.valor}
  `;
  alert(msgData);
}
